// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeTheGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKETHEGAME_API ASnakeTheGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
